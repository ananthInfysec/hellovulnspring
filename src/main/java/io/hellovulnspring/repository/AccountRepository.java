package io.hellovulnspring.repository;

import io.hellovulnspring.model.Account;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AccountRepository extends CrudRepository<Account, Long> {
  @Query(value = "SELECT id,type,routing_number,account_number,balance,interest FROM Account a WHERE a.type=:type ", nativeQuery = true)
  List<Account> getAccount(@Param("type") String accountType);

}
