package io.hellovulnspring.model;

import lombok.ToString;

@ToString
public class Constants {
	public enum Type {
		CHECKING, SAVING, MONEYMARKET
	}
}
